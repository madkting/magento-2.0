<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class CreateForm
 * @package Madkting\Connect\Controller\Adminhtml\Product
 */
class CreateForm extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * CreateForm constructor
     *
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $selectedProductsCount = 0;
        if (!empty($selected = $this->getRequest()->getParam(Filter::SELECTED_PARAM))) {
            $selectedProductsCount = count($selected);
        } else if (!empty($this->getRequest()->getParam(Filter::EXCLUDED_PARAM))) {
            $selectedProductsCount = 2;
        }

        if ($selectedProductsCount > 0) {
            $pageResult = $this->pageFactory->create();
            $pageResult->setActiveMenu('Magento_Catalog::catalog_products');
            $pageResult->getConfig()->getTitle()->prepend($this->getTitle($selectedProductsCount));

            return $pageResult;
        } else {
            $this->messageManager->addErrorMessage(__('No products selected'));
            return $this->resultRedirectFactory->create()->setPath('catalog/product');
        }
    }

    /**
     * Get form title
     *
     * @param int $products
     * @return string
     */
    protected function getTitle($products)
    {
        if ($products > 1) {
            $title = __('Create %1 on Madkting', __('Products'));
        } else {
            $title = __('Create %1 on Madkting', __('Product'));
        }

        return $title;
    }

    /*
	 * Check permission via ACL resource
	 */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Madkting_Connect::product_create');
    }
}
