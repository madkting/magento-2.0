<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Madkting Software (http://www.madkting.com)
  ~
  ~                                      ..-+::moossso`:.
  ~                                    -``         ``ynnh+.
  ~                                 .d                 -mmn.
  ~     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
  ~    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
  ~    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
  ~    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
  ~    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
  ~    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
  ~    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
  ~                                 .ys .::-`
  ~                                o.+`
  ~
  ~ @category Module
  ~ @package Madkting\Connect
  ~ @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
  ~ @author Israel Calderón Aguilar <israel@madkting.com>
  ~ @link https://bitbucket.org/madkting/magento2
  ~ @copyright Copyright (c) 2017 Madkting Software.
  ~ @license See LICENSE.txt for license details.
  -->

<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
    <system>
        <tab id="madkting" translate="label" sortOrder="199">
            <label>Madkting</label>
        </tab>
        <section id="madkting_configuration" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
            <class>separator-top</class>
            <label>Configuration</label>
            <tab>madkting</tab>
            <resource>Madkting_Connect::configuration</resource>
            <group id="general" translate="label comment" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                <label>General</label>
                <field id="api_token" translate="label comment" type="obscure" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>API Token</label>
                    <backend_model>Madkting\Connect\Model\Config\Backend\MadktingToken</backend_model>
                    <comment>
                        <![CDATA[<a href="https://software.madkting.com/" target="_blank">Click here to sign in to your Madkting account</a>]]>
                    </comment>
                </field>
                <field id="valid_token" translate="label" type="label" sortOrder="11" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Status</label>
                    <frontend_model>Madkting\Connect\Block\System\Config\Form\Field\ValidToken</frontend_model>
                </field>
                <field id="store_id" translate="label" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Store</label>
                    <source_model>Magento\Config\Model\Config\Source\Store</source_model>
                    <comment>
                        <![CDATA[Select store where Madkting will get products and update orders]]>
                    </comment>
                </field>
                <field id="webhook_url" translate="label" type="label" sortOrder="21" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Notifications URL</label>
                    <frontend_model>Madkting\Connect\Block\System\Config\Form\Field\WebhookUrl</frontend_model>
                </field>
            </group>
        </section>
        <section id="madkting_synchronization" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
            <class>separator-top</class>
            <label>Synchronization</label>
            <tab>madkting</tab>
            <resource>Madkting_Connect::synchronization</resource>
            <group id="orders" translate="label comment" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                <label>Orders</label>
                <field id="sync_orders" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Synchronize Orders</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="start_creation_from" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Start Creation From</label>
                    <frontend_model>Madkting\Connect\Block\Adminhtml\Config\DatePicker</frontend_model>
                    <depends>
                        <field id="sync_orders">1</field>
                    </depends>
                </field>
            </group>
            <group id="products" translate="label comment" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="0">
                <label>Products</label>
                <field id="permanent_sync" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Permanent Synchronization</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="upload_disabled" translate="label" type="select" sortOrder="11" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Upload Disabled Products</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="disable_prices_sync" translate="label" type="select" sortOrder="12" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Disable Price Synchronization</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="sync_all" translate="label" type="button" sortOrder="13" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Synchronize All</label>
                    <frontend_model>Madkting\Connect\Block\System\Config\Form\Field\SyncAll</frontend_model>
                    <comment>Synchronize all the data in all the products created in Madkting</comment>
                </field>
                <field id="sync_stock_prices" translate="label comment" type="button" sortOrder="14" showInDefault="1" showInWebsite="1" showInStore="0">
                    <label>Synchronize Stock and Price</label>
                    <frontend_model>Madkting\Connect\Block\System\Config\Form\Field\SyncStockPrices</frontend_model>
                    <comment>Synchronize only stock and price in all products created in Madkting</comment>
                </field>
            </group>
            <group id="tasks_queue" translate="label" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Tasks Queue</label>
                <field id="success_lifetime" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Success History Lifetime (minutes)</label>
                </field>
                <field id="failure_lifetime" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Failure History Lifetime (minutes)</label>
                </field>
            </group>
        </section>
    </system>
</config>
