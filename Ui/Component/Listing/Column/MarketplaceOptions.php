<?php
namespace Madkting\Connect\Ui\Component\Listing\Column;

use Madkting\Connect\Helper\Data;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class MarketplaceOptions
 * @package Madkting\Connect\Ui\Component\Listing\Column\Sales
 */
class MarketplaceOptions implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var Data
     */
    protected $madktingHelper;

    /**
     * MarketplaceOptions constructor.
     * @param Data $madktingHelper
     */
    public function __construct(
        Data $madktingHelper
    ) {
        $this->madktingHelper = $madktingHelper;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->getOptionArray();
        }
        return $this->options;
    }

    /**
     * @return array
     */
    private function getOptionArray()
    {
        $options = [];

        $marketplaces = $this->madktingHelper->getMarketplaces();

        if (!empty($marketplaces)) {
            foreach ($marketplaces as $code => $field) {
                $data['value'] = $code;
                $data['label'] = $field;

                $options[] = $data;
            }
        }

        return $options;
    }
}
