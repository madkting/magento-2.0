<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @author Israel Calderón Aguilar <israel@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Tax\Model\ClassModel;

/**
 * Class UpgradeData
 * @package Madkting\Connect\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Madkting's Name
     */
    const MADKTING_NAME = 'Madkting';

    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * InstallData constructor
     *
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory
    ) {
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * @inheritdoc
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $setup->getConnection();

        /* 0.0.2 version */
        if (version_compare($context->getVersion(), '0.0.2', '<')) {

            /* Mapping order status table data */
            $orderStatuses = [
                [
                    'status_madkting' => 'payment_required',
                    'status_magento' => Order::STATE_PENDING_PAYMENT,
                    'document' => 'N/A',
                    'create_document' => 0
                ],
                [
                    'status_madkting' => 'pending',
                    'status_magento' => 'pending',
                    'document' => 'N/A',
                    'create_document' => 0
                ],
                [
                    'status_madkting' => 'paid',
                    'status_magento' => Order::STATE_PROCESSING,
                    'document' => 'Invoice',
                    'create_document' => 1
                ],
                [
                    'status_madkting' => 'shipped',
                    'status_magento' => Order::STATE_COMPLETE,
                    'document' => 'Shipment',
                    'create_document' => 1
                ],
                [
                    'status_madkting' => 'delivered',
                    'status_magento' => Order::STATE_COMPLETE,
                    'document' => 'N/A',
                    'create_document' => 0
                ],
                [
                    'status_madkting' => 'failed_delivery',
                    'status_magento' => Order::STATE_COMPLETE,
                    'document' => 'N/A',
                    'create_document' => 0
                ],
                [
                    'status_madkting' => 'refunded',
                    'status_magento' => Order::STATE_CLOSED,
                    'document' => 'Credit Memo',
                    'create_document' => 1
                ],
                [
                    'status_madkting' => 'canceled',
                    'status_magento' => Order::STATE_CANCELED,
                    'document' => 'N/A',
                    'create_document' => 0
                ]
            ];
            $connection->insertMultiple($setup->getTable('madkting_mapping_order_status'), $orderStatuses);

            /* Add Madkting order status to Magento quote and order */
            $quoteSetup = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);
            $salesSetup = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);

            $attribute = [
                'code' => 'madkting_status',
                'params' => ['type' => 'varchar', 'visible' => false, 'length' => 50, 'comment' =>'Madkting Order ID']
            ];

            $quoteSetup->addAttribute('quote', $attribute['code'], $attribute['params']);
            $salesSetup->addAttribute('order', $attribute['code'], $attribute['params']);
        }

        /* 1.0.0 version */
        if (version_compare($context->getVersion(), '1.0.0', '<')) {

            /* Add unique index for madkting_pk on sales_order table */
            $connection->addIndex(
                $setup->getTable('sales_order'),
                $connection->getIndexName(
                    'sales_order',
                    'madkting_pk',
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                'madkting_pk',
                AdapterInterface::INDEX_TYPE_UNIQUE
            );

            /* Add Madkting marketplace order reference to Magento quote and order */
            $quoteSetup = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);
            $salesSetup = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);

            $attribute = [
                'code' => 'madkting_marketplace_reference',
                'params' => ['type' => 'varchar', 'visible' => false, 'length' => 255, 'comment' =>'Madkting Marketplace Order Reference']
            ];

            $quoteSetup->addAttribute('quote', $attribute['code'], $attribute['params']);
            $salesSetup->addAttribute('order', $attribute['code'], $attribute['params']);

            /* Madkting tax class */
            $taxTable = $setup->getTable('tax_class');
            $connection->insert(
                $taxTable,
                [
                    'class_name' => self::MADKTING_NAME,
                    'class_type' => ClassModel::TAX_CLASS_TYPE_CUSTOMER
                ]
            );
            $taxClassId = $connection->lastInsertId($taxTable);

            /* Madkting customer group */
            if (!empty($taxClassId)) {
                $connection->insert(
                    $setup->getTable('customer_group'),
                    [
                        'customer_group_code' => self::MADKTING_NAME,
                        'tax_class_id' => $taxClassId
                    ]
                );
            }

            /* Creation of last synchronization date field */
            $connection->insert($setup->getTable('core_config_data'), [
                'path' => 'madkting_synchronization/products/last_sync_date',
                'value' => '0'
            ]);
        }

        /* 1.0.2 version */
        if (version_compare($context->getVersion(), '1.0.2', '<')) {

            /* Update order status mapping data */
            $orderStatusTable = $setup->getTable('madkting_mapping_order_status');
            $rows = [
                'payment_required' => [
                    'document' => null
                ],
                'pending' => [
                    'document' => null
                ],
                'paid' => [
                    'document' => 'invoice'
                ],
                'shipped' => [
                    'document' => 'shipment'
                ],
                'delivered' => [
                    'document' => null
                ],
                'failed_delivery' => [
                    'document' => null
                ],
                'refunded' => [
                    'document' => 'credit_memo'
                ],
                'canceled' => [
                    'document' => 'credit_memo',
                    'create_document' => 1
                ]
            ];
            foreach ($rows as $madktingStatus => $data) {
                $connection->update($orderStatusTable, $data, [
                    'status_madkting = ?' => $madktingStatus
                ]);
            }
        }

        $setup->endSetup();
    }
}
