<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Israel Calderón Aguilar <israel@madkting.com>
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Model\CategoriesMapping;

use Madkting\Connect\Model\ResourceModel\CategoriesMapping\Collection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Framework\Data\OptionSourceInterface;

class MagentoCategoriesOptions implements OptionSourceInterface
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var array
     */
    protected $categories;

    /**
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param RequestInterface $request
     */
    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        RequestInterface $request
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getFormattedCategories();
    }

    /**
     * Retrieve categories tree
     *
     * @return array
     */
    protected function getFormattedCategories()
    {
        $this->getSearchableCategoriesArray();

        $formattedArray = [];

        foreach ($this->categories as $key => $value) {
            $formattedArray[] = [
                'value' => $key,
                'label' => $this->getParentsName($value['id_parent'], $value['name'])
            ];
        }

        /* Order by parent */
        usort($formattedArray, function($a, $b){

            $order = strcasecmp($a['label'], $b['label']);

            if ($order < 0) return -1;
            elseif ($order > 0) return 1;
            else return 0;
        });

        return $formattedArray;
    }

    /**
     * @return array
     */
    public function getSearchableCategoriesArray()
    {
        if (empty($this->categories)) {
            $searchableArray = [];

            $storeId = $this->request->getParam('store');
            /* @var $matchingNamesCollection \Magento\Catalog\Model\ResourceModel\Category\Collection */
            $matchingNamesCollection = $this->categoryCollectionFactory->create();

            $matchingNamesCollection->addAttributeToSelect('path')
                ->addAttributeToFilter('entity_id', ['neq' => CategoryModel::TREE_ROOT_ID])
                ->setStoreId($storeId);

            $shownCategoriesIds = [];

            /** @var \Magento\Catalog\Model\Category $category */
            foreach ($matchingNamesCollection as $category) {
                foreach (explode('/', $category->getPath()) as $parentId) {
                    $shownCategoriesIds[$parentId] = 1;
                }
            }

            /* @var $collection \Magento\Catalog\Model\ResourceModel\Category\Collection */
            $collection = $this->categoryCollectionFactory->create();

            $collection->addAttributeToFilter('entity_id', ['in' => array_keys($shownCategoriesIds)])
                ->addAttributeToSelect(['name', 'is_active', 'parent_id'])
                ->setStoreId($storeId);

            foreach ($collection as $category) {
                if ($category->getParentId() != 0) {
                    $searchableArray[$category->getId()] = [
                        'name' => $category->getName(),
                        'id_parent' => $category->getParentId() != 1 ? $category->getParentId() : null
                    ];
                }
            }

            $this->categories = $searchableArray;
        }

        return $this->categories;
    }

    /**
     * @param int|null $parentId
     * @param string $label
     * @return string
     */
    public function getParentsName($parentId = null, $label = '')
    {
        if(is_null($parentId) || $parentId == 2){
            return $label;
        }else{
            return $this->getParentsName(
                $this->categories[$parentId]['id_parent'],
                $this->categories[$parentId]['name'] . ' > ' . $label
            );
        }
    }

    /**
     * @param array $categoriesArray
     * @return $this
     */
    public function setCategories($categoriesArray)
    {
        $this->categories = $categoriesArray;

        return $this;
    }
}
