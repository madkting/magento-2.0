<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @author Israel Calderón Aguilar <israel@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Model;

use Madkting\MadktingClient;
use Magento\Config\Model\Config\Factory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\Encryptor;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * @package Madkting\Connect\Model
 */
class Config
{
    /**
     * Configuration paths
     */
    const MADKTING_PATH = 'madkting_configuration/';
    const GENERAL_PATH = 'general/';
    const MADKTING_SYNC_PATH = 'madkting_synchronization/';
    const SYNC_ORDERS_PATH = 'orders/';
    const SYNC_PRODUCTS_PATH = 'products/';
    const TASKS_QUEUE = 'tasks_queue/';

    /**
     * @var ScopeConfigInterface
     */
    protected $config;
    /**
     * @var Encryptor
     */
    protected $encryptor;
    /**
     * @var Factory
     */
    protected $configFactory;
    /**
     * The value in second or unix of 24 hours
     * @const INT
     */
    const ONE_DAY = 86400;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $config
     * @param Encryptor $encryptor
     */
    public function __construct(
        ScopeConfigInterface $config,
        Encryptor $encryptor,
        Factory $configFactory
    ) {
        $this->config = $config;
        $this->encryptor = $encryptor;
        $this->configFactory = $configFactory;
    }

    /**
     * Get Madkting API token
     *
     * @return string|bool
     */
    public function getMadktingToken()
    {
        /* Validate token */
        $token = $this->encryptor->decrypt($this->config->getValue(self::MADKTING_PATH . self::GENERAL_PATH . 'api_token', ScopeInterface::SCOPE_STORE));
        try {
            $client = new MadktingClient(['token' => $token]);
            $client->testToken();

            return $token;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get Magento store selected to integrate with Madkting
     *
     * @return int
     */
    public function getSelectedStore()
    {
        return $this->config->getValue(self::MADKTING_PATH . self::GENERAL_PATH . 'store_id');
    }

    /**
     * @return bool
     */
    public function isSynchronizeOrdersEnabled()
    {
        return (bool)$this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_ORDERS_PATH . 'sync_orders');
    }

    public function getStartCreationOrderDate()
    {
        return $this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_ORDERS_PATH . 'start_creation_from');
    }

    /**
     * @return bool
     */
    public function isPermanentSynchronizationEnabled()
    {
        return (bool)$this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_PRODUCTS_PATH . 'permanent_sync');
    }

    /**
     * @return bool
     */
    public function isUploadDisabledProductsEnabled()
    {
        return (bool)$this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_PRODUCTS_PATH . 'upload_disabled');
    }

    /**
     * @return bool
     */
    public function isDisablePriceSynchronizationEnabled()
    {
        return (bool)$this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_PRODUCTS_PATH . 'disable_prices_sync');
    }

    /**
     * @return int
     */
    public function getLastSyncDate()
    {
        return (int)$this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_PRODUCTS_PATH . 'last_sync_date');
    }

    /**
     * @return int
     */
    public function getNextSyncDate()
    {
        return ((int)$this->config->getValue(self::MADKTING_SYNC_PATH . self::SYNC_PRODUCTS_PATH . 'last_sync_date') + self::ONE_DAY);
    }

    /**
     * @return int
     */
    public function getSyncTimeLeft()
    {
        return $this->getNextSyncDate() - time();
    }

    /**
     * Save current time stamp in unix format
     * @return void
     */
    public function setLastSyncDate()
    {
        $configuration = $this->configFactory->create();
        $configuration->setDataByPath(self::MADKTING_SYNC_PATH . self::SYNC_PRODUCTS_PATH . 'last_sync_date', time() );
        $configuration->save();
    }

    /**
     * Get tasks queue success history lifetime
     *
     * @return int
     */
    public function getTasksSuccessHistoryLifetime()
    {
        return (int) $this->config->getValue(self::MADKTING_SYNC_PATH . self::TASKS_QUEUE . 'success_lifetime');
    }

    /**
     * Get tasks queue failure history lifetime
     *
     * @return int
     */
    public function getTasksFailureHistoryLifetime()
    {
        return (int) $this->config->getValue(self::MADKTING_SYNC_PATH . self::TASKS_QUEUE . 'failure_lifetime');
    }
}
