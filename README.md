Madkting - Magento 2 Module
=========

Connect and synchronize your Magento 2 store with the biggest online Marketplaces in LATAM.

Installation
---------

1. Add the dependency in your composer config

    ```
    composer require madkting/magento2.0-module
    ```
    
2. Enable it in Magento

    ```
    php bin/magento module:enable Madkting_Connect
    ```

3. Upgrade your Magento

    ```
    php bin/magento setup:upgrade
    ```

Updates
---------

* For update this module execute the next command

    ```
    composer update madkting/magento2.0-module
    ```
