<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Block\Adminhtml\Product\Form;

use Madkting\Connect\Model\Config;
use Madkting\MadktingClient;
use Magento\Backend\Block\Widget;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Create
 * @package Madkting\Connect\Block\Adminhtml\Product\Form
 */
class Create extends Widget\Container
{
    /**
     * @var array
     */
    protected $selectedProducts = [];

    /**
     * @var array
     */
    protected $excludedProducts = [];

    /**
     * @var array
     */
    protected $madktingShops = [];

    /**
     * @var Config
     */
    protected $config;

    /**
     * Create constructor.
     * @param Widget\Context $context
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Widget\Context $context,
        Config $config,
        array $data = []
    ) {
        $this->config = $config;
        parent::__construct($context, $data);

        /* Add grid buttons */
        $this->addButtons();
    }

    /**
     * Add form buttons
     */
    protected function addButtons()
    {
        /* Add buttons */
        $this->addButton(
            'back_button',
            [
                'label' => __('Back'),
                'class' => 'back',
                'onclick' => 'setLocation(\'' . $this->getBackAction() . '\')'
            ]
        );

        $this->addButton(
            'create_products_button',
            [
                'label' => $this->getCreateButtonLabel(),
                'class' => 'primary'
            ]
        );
    }

    /**
     * Back controller
     *
     * @return string
     */
    protected function getBackAction()
    {
        return $this->getUrl('catalog/product/');
    }

    /**
     * @return string
     */
    protected function getCreateButtonLabel()
    {
        if (count($this->getSelectedProducts()) > 1) {
            $title = __('Create %1', __('Products'));
        } else {
            $title = __('Create %1', __('Product'));
        }

        return $title;
    }

    /**
     * @return array
     */
    public function getSelectedProducts()
    {
        if (empty($this->selectedProducts)) {
            $this->selectedProducts = $this->getRequest()->getParam(Filter::SELECTED_PARAM);
        }

        return $this->selectedProducts;
    }

    /**
     * @return array
     */
    public function getExcludedProducts()
    {
        if (empty($this->excludedProducts)) {
            $this->excludedProducts = $this->getRequest()->getParam(Filter::EXCLUDED_PARAM);
        }

        return $this->excludedProducts;
    }

    /**
     * Get Madkting Shops available for token
     *
     * @return array
     */
    public function getMadktingShops()
    {
        if (empty($this->madktingShops)) {
            $token = $this->config->getMadktingToken();
            if ($token) {
                $client = new MadktingClient(['token' => $token]);
                $shopService = $client->serviceShop();
                $this->madktingShops = $shopService->search();
            }
        }

        return $this->madktingShops;
    }

    /**
     * If upload disabled products option is enabled
     *
     * @return bool
     */
    public function isUploadDisabledProductsEnabled()
    {
        return $this->config->isUploadDisabledProductsEnabled();
    }
}
