<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Block\Adminhtml\CategoriesMapping;


use Madkting\Connect\Block\Adminhtml\MadktingGrid;
use Madkting\Connect\Model\CategoriesMapping\MadktingCategoriesOptions;
use Madkting\Connect\Model\CategoriesMapping\MagentoCategoriesOptions;
use Madkting\Connect\Model\ResourceModel\CategoriesMapping\CollectionFactory;
use Magento\Backend\Block\Widget\Context;
use Magento\Catalog\Model\CategoryFactory;

/**
 * Class Grid
 * @package Madkting\Connect\Block\Adminhtml\CategoriesMapping
 */
class Grid extends MadktingGrid
{
    /**
     * @var string
     */
    protected $gridName = 'madkting-category';
    /**
     * @var CollectionFactory
     */
    private $catMapCollection;
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;
    /**
     * @var MadktingCategoriesOptions
     */
    private $madktingCategoriesOptions;
    /**
     * @var MagentoCategoriesOptions
     */
    private $magentoCategoriesOptions;

    /**
     * Grid constructor
     *
     * @param Context $context
     * @param array $data
     * @param CollectionFactory $catMapCollection
     * @param CategoryFactory $categoryFactory
     * @param MadktingCategoriesOptions $madktingCategoriesOptions
     * @param MagentoCategoriesOptions $magentoCategoriesOptions
     */
    public function __construct(
        Context $context,
        array $data = [],
        CollectionFactory $catMapCollection,
        CategoryFactory $categoryFactory,
        MadktingCategoriesOptions $madktingCategoriesOptions,
        MagentoCategoriesOptions $magentoCategoriesOptions
    ){
        parent::__construct($context, $data);
        $this->catMapCollection = $catMapCollection;
        $this->categoryFactory = $categoryFactory;
        $this->madktingCategoriesOptions = $madktingCategoriesOptions;
        $this->magentoCategoriesOptions = $magentoCategoriesOptions;
        $this->addButtons();
        $this->setColumns();
        $this->setRows();
    }

    protected function addButtons()
    {
        $this->addButton(
            'new-category-mapping',
            [
                'label' => __('New Category Mapping'),
                'class' => 'primary',
                'onclick' => 'setLocation(\'' . $this->getUrl('madkting/categoriesmapping/create') . '\')',
            ]
        );
    }

    protected function setColumns()
    {
        $this->addColumn('madkting_category_name', __('Madkting category'));
        $this->addColumn('magento_categories', __('Magento categories'));
        $this->addColumn('actions', __('Actions'));
    }

    protected function setRows()
    {
        $categoriesMapped = $this->catMapCollection->create()->getItems();

        $madktingCategories = $this->madktingCategoriesOptions->getSearchableCategoriesArray();
        $this->madktingCategoriesOptions->setCategories($madktingCategories);

        $magentoCategories = $this->magentoCategoriesOptions->getSearchableCategoriesArray();
        $this->magentoCategoriesOptions->setCategories($magentoCategories);

        foreach ($categoriesMapped as $match){

            $madktingCategoryName = $this->madktingCategoriesOptions->getParentsName($madktingCategories[$match->getMadktingCategoryId()]['id_parent'], $madktingCategories[$match->getMadktingCategoryId()]['name']);
            $magentoCategoryName = $this->magentoCategoriesOptions->getParentsName($magentoCategories[$match->getMagentoCategoryId()]['id_parent'], $magentoCategories[$match->getMagentoCategoryId()]['name']);

            $this->addRow([
                [
                    'columnCode' => 'madkting_category_name',
                    'value' => $madktingCategoryName
                ],
                [
                    'columnCode' => 'magento_categories',
                    'value' => $magentoCategoryName
                ],
                [
                    'columnCode' => 'actions',
                    'value' => '<a href="' . $this->getDeleteUrl($match->getMagentoCategoryId()) . '">' . __('Delete') . '</a>',
                    'class' => 'data-grid-actions-cell'
                ]
            ]);
        }

    }

    /**
     * @param $magentoCategoryId
     * @return string
     */
    protected function getDeleteUrl($magentoCategoryId)
    {
        return $this->_urlBuilder->getUrl('*/categoriesmapping/delete', ['id' => $magentoCategoryId]);
    }
}