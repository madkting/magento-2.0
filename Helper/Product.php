<?php
/**
 * Madkting Software (http://www.madkting.com)
 *
 *                                      ..-+::moossso`:.
 *                                    -``         ``ynnh+.
 *                                 .d                 -mmn.
 *     .od/hs..sd/hm.   .:mdhn:.   yo                 `hmn. on     mo omosnomsso oo  .:ndhm:.   .:odhs:.
 *    :hs.h.shhy.d.mh: :do.hd.oh:  /h                `+nm+  dm   ys`  ````mo```` hn :ds.hd.yo: :oh.hd.dh:
 *    ys`   `od`   `h+ sh`    `do  .d`              `snm/`  +s hd`        hd     yy yo`    `sd oh`    ```
 *    hh     sh     +m hs      yy   y-            `+mno`    dkdm          +d     o+ no      ss ys    dosd
 *    y+     ss     oh hdsomsmnmy   ++          .smh/`      om ss.        dh     mn yo      oh sm      hy
 *    sh     ho     ys hs``````yy   .s       .+hh+`         ys   hs.      os     yh os      d+ od+.  ./m/
 *    od     od     od od      od   +y    .+so:`            od     od     od     od od      od  `syssys`
 *                                 .ys .::-`
 *                                o.+`
 *
 * @category Module
 * @package Madkting\Connect
 * @author Carlos Guillermo Jiménez Salcedo <guillermo@madkting.com>
 * @link https://bitbucket.org/madkting/magento2
 * @copyright Copyright (c) 2017 Madkting Software.
 * @license See LICENSE.txt for license details.
 */

namespace Madkting\Connect\Helper;

use Madkting\Connect\Model\ProductTaskQueue;
use Magento\ConfigurableProduct\Model\Product\Type\ConfigurableFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\InputException;

/**
 * Class Product
 * @package Madkting\Connect\Helper
 */
class Product extends AbstractHelper
{
    /**
     * Magento Product ID
     *
     * @var int
     */
    protected $productId;

    /**
     * @var ConfigurableFactory
     */
    protected $configurableProductFactory;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $configurableModel;

    /**
     * Product constructor
     *
     * @param Context $context
     * @param ConfigurableFactory $configurableProductFactory
     */
    public function __construct(
        Context $context,
        ConfigurableFactory $configurableProductFactory
    ) {
        parent::__construct($context);
        $this->configurableProductFactory = $configurableProductFactory;
    }

    /**
     * @param int|null $productId
     * @return $this
     * @throws InputException
     */
    public function setProductId($productId = null)
    {
        if (!empty($productId)) {
            $this->productId = $productId;
        }

        if (empty($this->productId)) {
            throw new InputException(__('No product ID information'));
        }

        return $this;
    }

    /**
     * @param int|null $productId
     * @return int
     * @throws InputException
     */
    public function getProductType($productId = null)
    {
        return !empty($this->getParentId($productId)) ? ProductTaskQueue::TYPE_VARIATION : ProductTaskQueue::TYPE_PRODUCT;
    }

    /**
     * @param int|null $productId
     * @return int|false
     * @throws InputException
     */
    public function getParentId($productId = null)
    {
        $this->setProductId($productId);

        $parentId = $this->getConfigurableModel()->getParentIdsByChild($this->productId);

        return !empty($parentId) ? $parentId[0] : false;
    }

    /**
     * @param int|null $productId
     * @return bool
     * @throws InputException
     */
    public function hasVariations($productId = null)
    {
        return !empty($this->getVariationsId($productId)) ? true : false;
    }

    /**
     * @param int|null $productId
     * @return array
     * @throws InputException
     */
    public function getVariationsId($productId = null)
    {
        $this->setProductId($productId);

        return $this->getConfigurableModel()->getChildrenIds($this->productId)[0];
    }

    /**
     * Get instance of configurable model
     *
     * @return \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected function getConfigurableModel()
    {
        if (empty($this->configurableModel)) {
            $this->configurableModel = $this->configurableProductFactory->create();
        }

        return $this->configurableModel;
    }
}
